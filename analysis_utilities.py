import os
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pandas as pd
import xlsxwriter

def load_timeseries(vname,cm,sc,y,yoff):
    #Load 30 year climate window
    fname=os.path.join("data/Yellowknife-A/PCIC",cm+"_"+sc+"_BCCAQ_"+vname+".nc")
    df=xr.open_dataset(fname)
    ds=np.squeeze(df[vname])
    ts=ds.loc[str(y-yoff)+'-01-01':str(y)+'-12-31']
    return ts

def load_full_timeseries(vname,cm,sc):
    #Load entire time series
    fname=os.path.join("data/Yellowknife-A/PCIC",cm+"_"+sc+"_BCCAQ_"+vname+".nc")
    df=xr.open_dataset(fname)
    ts=np.squeeze(df[vname])
    return ts

def make_monthly_climos(ts):
    
    #Make monthly climatologies 
    m_mean=np.zeros(12)
    #m_std=np.zeros(12) 
    m_sum=np.zeros(12)
    #m_10=np.zeros(12)
    #m_90=np.zeros(12)
    #m_33=np.zeros(12)
    #m_67=np.zeros(12)
    for m in range(0,12):
        #Group all input data by month, then get stats for each month
        i=ts.groupby('time.month').groups[m+1]
        m_mean[m]=ts[i].mean()
        #m_std[m]  =ts[i].std()
        m_sum[m]  =ts[i].sum()
        #m_10[m]   =np.percentile(ts[i],10)
        #m_90[m]   =np.percentile(ts[i],90)
        #m_33[m]   =np.percentile(ts[i],33)
        #m_67[m]   =np.percentile(ts[i],67)
    return m_mean,m_sum #Just return 

def make_climo_plot(c,lb,ub,col,y):
    #Make plot
    x=np.arange(0,12)
    l=plt.plot(x,c,col,label=str(y))
    plt.plot(x,ub,col,linestyle='--')
    #if y==ay[0] or y==ay[-1]:
    plt.fill_between(x,lb,ub,where=lb<ub,facecolor=col,interpolate=True,alpha=0.4)    
    return l

def plot_seasonal_cycle(l1,lab1,l2,lab2,ay,YaxisTitle,FigPath,FigPrefix,ymin,ymax):
    for i in range(len(ay)):
        cay="firebrick"
        fig=plt.figure()
        yr=ay[i]
        x=np.arange(0,12)
        plt.plot(x,l1[:,i],cay,label=lab1+", "+str(yr))
        plt.plot(x,l2[:,i],cay,linestyle='--',linewidth=2,label=lab2+", "+str(yr))
        plt.legend()
        labels=['Jan','','','Apr','','','Jul','','','Oct','','']
        plt.xticks(np.arange(0,12), labels, rotation='vertical')
        plt.xlim([0,11])
        plt.ylim([ymin,ymax])
        plt.ylabel(YaxisTitle)
        plt.grid(linestyle="--")
        plt.savefig(FigPath+FigPrefix+str(yr)+".png",dpi=300)
        plt.close("all")

def generate_data_table(writer,FutProjThresh,TitleThresh,ay):
    Months=['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    df=pd.DataFrame(index=Months,data=FutProjThresh)
    df.to_excel(writer,TitleThresh,index=True,header=ay)

    return writer

def calculate_rain_snow_frac(tasmin,tasmax,pr):
    RainSnow=pr#steal the attributes from pr
    RainSnow=tasmax/(tasmax-tasmin)#approaches 1 as fraction falling as rain increases.  This is to estimate sub-daily snow/rain fractionation
    RainSnow[RainSnow<0.]=0.
    RainSnow[RainSnow>1.]=1.
    RainFlux=pr*RainSnow#total rainflux
    SnowFlux=pr-RainFlux#total snowflux
    return RainFlux,SnowFlux,RainSnow

def calculate_projections(ModelData,ObservedData,ds):
    dp50=np.zeros(ds)#Month, analysis year
    dp55=np.zeros(ds)
    dp67=np.zeros(ds)
    dp75=np.zeros(ds)
    dp85=np.zeros(ds)

    FutProj50=np.zeros(ds)
    FutProj55=np.zeros(ds)
    FutProj67=np.zeros(ds)
    FutProj75=np.zeros(ds)
    FutProj85=np.zeros(ds)
    #p50=np.percentile(ModelData,50,axis=(0,1,2))
    p50=np.mean(ModelData,axis=(0,1,2)) #Note, terminology is a bit off here since using mean, but calling p50
    p55,p67,p75,p85=np.percentile(ModelData,[55,67,75,85],axis=(0,1,2))
    for i in range(ds[1]):
        dp50[:,i]=p50[:,i]-p50[:,0]
        dp55[:,i]=p55[:,i]-p50[:,0]
        dp67[:,i]=p67[:,i]-p50[:,0]
        dp75[:,i]=p75[:,i]-p50[:,0]
        dp85[:,i]=p85[:,i]-p50[:,0]
        FutProj50[:,i]=ObservedData+dp50[:,i]
        FutProj55[:,i]=ObservedData+dp55[:,i]
        FutProj67[:,i]=ObservedData+dp67[:,i]
        FutProj75[:,i]=ObservedData+dp75[:,i]
        FutProj85[:,i]=ObservedData+dp85[:,i]
    return dp50,dp55,dp67,dp75,dp85,FutProj50,FutProj55,FutProj67,FutProj75,FutProj85
