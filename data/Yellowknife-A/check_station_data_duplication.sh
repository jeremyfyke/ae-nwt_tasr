#!/bin/bash

#Run through all files and double check that all monthly files within a year are in fact identical
#Silence means all monthly files within a year match.

#NOTE: since 2013 year overlaps between datasets and I've done some manual editing, differences will occur here.  I've removed this year
#From this checking script (but also checked prior to editing for similarity, they were similar)
for s in 1706; do
    for yr in {1980..2012}; do
        fbase=Yellowknife-A-"$s"/"$s"_"$yr"_1.csv
	for f in {2..12}; do
            fcomp=Yellowknife-A-"$s"/"$s"_"$yr"_"$f".csv
	    diff $fbase $fcomp
	done
    done
done


for s in 51058; do
    for yr in {2014..2017}; do
        fbase=Yellowknife-A-"$s"/"$s"_"$yr"_1.csv
	for f in {2..12}; do
            fcomp=Yellowknife-A-"$s"/"$s"_"$yr"_"$f".csv
	    diff $fbase $fcomp
	done
    done
done

