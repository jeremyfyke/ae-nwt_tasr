#!/bin/bash

http=http://tools.pacificclimate.org/dataportal/data/downscaled_gcms/
iloc=[0:55152][262:262][292:292]

#Original PCIC command http://tools.pacificclimate.org/dataportal/data/downscaled_gcms/pr+tasmax+tasmin_day_BCSD+ANUSPLIN300+CanESM2_historical+rcp85_r1i1p1_19500101-21001231.nc.nc?pr[0:55152][262:262][292:292]&

cm=( HadGEM2-ES ) #CCSM4 CanESM2 )
sc=45
ds=BCSD
en=( r1i1p1 ) #r2i1p1 r1i1p1 )
dr=( 19500101-21001230 ) #19500101-21001231 19500101-21001231)
for i in "${!cm[@]}"; do
for v in tasmin tasmax pr; do
	fname="$http"pr+tasmax+tasmin_day_"$ds"+ANUSPLIN300+"${cm[$i]}"_historical+rcp"$sc"_"${en[$i]}"_"${dr[$i]}".nc.nc?"$v""$iloc"
	fname_trimmed="$v"_"$ds"+ANUSPLIN300+"${cm[$i]}"_historical+rcp"$sc"_"${en[$i]}"_"${dr[$i]}".nc
	if [ ! -f $fname_trimmed ]; then
		echo Downloading $fname to $fname_trimmed ...
			wget -O $fname_trimmed $fname
	fi
done
done

