import pandas as pd
import glob
import matplotlib.pyplot as plt 
from analysis_utilities import calculate_rain_snow_frac
from natsort import natsorted


plt.switch_backend('agg') 

def load_Yellowknife_station_data():

    #LoadYellowknife-A station data from both datasets.
    #Note 1: _1 used to only load January file from each year, given that
    #all monthly files for each year are identical.  This was checked via diff script.
    #Note 2: Years to 2012 were picked from 1706 dataset and years 2013-2017 were picked
    #from from 51058 dataset.  Former/latter has/does not have first ~2 weeks of January.
    #It appears a switch of some sort happened at this time.  To resolve this, both 2013 datasets
    #(from both station datasets, for January year) were editted to remove missing data entries 
    #for periods where data was present in other file.
    df = pd.DataFrame()
    DataDir="/home/garmeson_lanl/NWT_TASR/data/Yellowknife-A/Yellowknife-A-1706/"
    flist=glob.glob(DataDir+"1706_19*_1.csv")
    flist.extend(glob.glob(DataDir+"1706_200*_1.csv"))
    flist.extend(glob.glob(DataDir+"1706_201[0-3]*_1.csv"))
    DataDir="/home/garmeson_lanl/NWT_TASR/data/Yellowknife-A/Yellowknife-A-51058/"
    flist.extend(glob.glob(DataDir+"51058_*201[3-7]*_1.csv"))
    for f in natsorted(flist):
        df=df.append(pd.read_csv(f))
    tmp1,tmp2,RS=calculate_rain_snow_frac(df["Min Temp (\xc2\xb0C)"],df["Max Temp (\xc2\xb0C)"],df["Total Precip (mm)"])
    df["Rain snow fraction (%)"]=RS
    
    #Create date vector as index
    df["Date/Time"]=pd.to_datetime(df["Date/Time"])
    df=df.set_index("Date/Time")

    
    #Calculate mean monthly climatology
    #Group daily data by month, over defined period
    Tstart="1988-01-01"
    Tend="2017-12-31"
    MonthlyDataGrouping=df.loc[Tstart:Tend].groupby('Month')
    
    #QA/QC: Calculate data completeness (% of days with existing data over defined period)
    nTot=MonthlyDataGrouping["Year"].count()#Assume no missing data for Year vector.
    TempDataPresent=MonthlyDataGrouping["Mean Temp (\xc2\xb0C)"].count()/nTot *  100.
    PrecipDataPresent=MonthlyDataGrouping["Total Precip (mm)"].count()/nTot * 100.
    RainSnowDataPresent=MonthlyDataGrouping["Rain snow fraction (%)"].count()/nTot * 100.
    
    #Calculate monthly means for each 
    Temperature=MonthlyDataGrouping["Mean Temp (\xc2\xb0C)"].mean()
    Precipitation=MonthlyDataGrouping["Total Precip (mm)"].mean()
    RainSnow=MonthlyDataGrouping["Rain snow fraction (%)"].mean()

    FigDir="/home/garmeson_lanl/bucket_mount/figs-Yellowknife-A/"

    #ax=df["Mean Temp (\xc2\xb0C)"].plot()
    #ax.set_xlabel("Time")
    #ax.set_ylabel("mean daily temperature (C)")
    #fig=ax.get_figure()
    #fig.savefig(FigDir+"Yellowknife-A-Temperature.png",dpi=300)
    #plt.close("all")
    
    #ax=df["Total Precip (mm)"].plot()
    #ax.set_xlabel("Time")
    #ax.set_ylabel("total daily precip (mm)")
    #fig=ax.get_figure()
    #fig.savefig(FigDir+"Yellowknife-A-Precip.png",dpi=300)
    #plt.close("all")

    ax=Temperature.plot()
    ax.set_ylabel("mean monthly temperature (C)\n"+Tstart+' to '+Tend)
    ax.set_ylim([-30,20])
    ax.grid(linestyle="--")
    fig=ax.get_figure()
    fig.savefig(FigDir+"Temp_climo.png",dpi=300)
    plt.close("all")

    ax=Precipitation.plot()
    ax.set_ylabel("mean monthly precipitation (mm/day)\n"+Tstart+' to '+Tend)
    ax.grid(linestyle="--")
    ax.set_ylim([0.0,3.0])
    fig=ax.get_figure()
    fig.savefig(FigDir+"Precip_climo.png",dpi=300)
    plt.close("all")

    ax=RainSnow.plot()
    ax.set_ylabel("mean monthly rain/snow fraction (%)\n"+Tstart+' to '+Tend)
    ax.grid(linestyle="--")
    ax.set_ylim([0.0,1.0])
    fig=ax.get_figure()
    fig.savefig(FigDir+"Rainsnow_climo.png",dpi=300)
    plt.close("all")

    ax=TempDataPresent.plot(label="Temperature")
    ax=PrecipDataPresent.plot(label="Precipitation")
    ax.set_ylabel("Data Present (%)\n"+Tstart+' to '+Tend)
    ax.grid(linestyle="--")
    ax.legend()
    fig=ax.get_figure()
    fig.savefig(FigDir+"DataPresent.png",dpi=300)
    plt.close("all")

    return Temperature,Precipitation,RainSnow

if __name__ == '__main__':
    load_Yellowknife_station_data()
