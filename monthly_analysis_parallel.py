import os
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pandas as pd
from analysis_utilities import load_timeseries, load_full_timeseries, make_monthly_climos, calculate_rain_snow_frac, calculate_projections
from analysis_utilities import plot_seasonal_cycle, generate_data_table
from load_station_data import load_Yellowknife_station_data
from functools import partial
import multiprocessing as mp
import time

#PCIC X, Y: 257, 318

LoadFreshData=True

DaysPerMonth=np.array([31.,28.25,31.,30.,31.,30.,31.,31.,30.,31.,30.,31.])

#Extract station data in monthly climatology form
ObsTemp,ObsPrecip,ObsRainFrac=load_Yellowknife_station_data()

ObsPrecip=ObsPrecip*DaysPerMonth

ObsRainFrac=ObsRainFrac*100.

#ay=[2017,2050]
#ay=[2017,2020,2025,2030,2035,2040,2045,2050]
ay=range(2017,2051)
nay=len(ay)
cay=['firebrick']*nay

ClimoWin=30
yoff=ClimoWin-1
FigPath="/home/garmeson_lanl/bucket_mount/figs_PA3.0/"
DataTablePath="/home/garmeson_lanl/bucket_mount/datatable_PA3.0/"
writer=pd.ExcelWriter(DataTablePath+"TASR_climate_projection.xlsx")

Scenarios=['45','85']
nSc=len(Scenarios)

#Scenarios=['45']
Models=["CCSM4","CanESM2","ACCESS1-0","inmcm4","CNRM-CM5","MRI-CGCM3","MPI-ESM-LR","CSIRO-Mk3-6-0","GFDL-ESM2G","MIROC5"]
#Models=["CCSM4","CanESM2"]
nMod=len(Models)
nMonthsinYear=12

MonthlyTemp=np.zeros((ClimoWin,len(Models),len(Scenarios),12,nay)) #window year,model, scenario, month, present/2080
MonthlyPrecip=np.zeros((ClimoWin,len(Models),len(Scenarios),12,nay))
MonthlyRainFrac=np.zeros((ClimoWin,len(Models),len(Scenarios),12,nay))
dMonthlyTemp=np.zeros((ClimoWin,len(Models),len(Scenarios),12,nay))
dMonthlyPrecip=np.zeros((ClimoWin,len(Models),len(Scenarios),12,nay)) 
dMonthlyRainFrac=np.zeros((ClimoWin,len(Models),len(Scenarios),12,nay))

def calc_climate(yr,tasmin_full,tasmax_full,pr_full):
   #Get daily time series, right-referenced for yoff-length window
   tasmin=tasmin_full.loc[str(yr-yoff)+'-01-01':str(yr)+'-12-31']
   tasmax=tasmax_full.loc[str(yr-yoff)+'-01-01':str(yr)+'-12-31']
   tas=(tasmin+tasmax)/2
   pr=pr_full.loc[str(yr-yoff)+'-01-01':str(yr)+'-12-31']
   
   rf,sf,rfrac=calculate_rain_snow_frac(tasmin,tasmax,pr)#calculates fluxes at daily frequency
   #loop over years in window period and extract data over year
   t=np.zeros((ClimoWin,nMonthsinYear))
   p=np.zeros((ClimoWin,nMonthsinYear))
   rf=np.zeros((ClimoWin,nMonthsinYear))
   nyr=0
   for nyr,yrsub in enumerate(range(yr-yoff,yr+1)):
       ts=pd.datetime(yrsub,1,1)
       te=pd.datetime(yrsub,12,31)
       #For all: 
       #  -gather data for individual years
       #  -generate monthly-averaged values for each year
       #  -accrue monthly averages in data arrays
       ##TEMPERATURE
       c,s=make_monthly_climos(tas.loc[ts:te])
       t[nyr,:]=c
       ##PRECIPITATION
       c,s=make_monthly_climos(pr.loc[ts:te])
       p[nyr,:]=c
       
       ##RAIN/SNOW FRACTION
       c,s=make_monthly_climos(rfrac.loc[ts:te])
       rf[nyr,:]=c 
       #c,s,su,p10,p90,p33,p67=make_monthly_climos(rfrac[np.where(pr>0.)])
   return t,p,rf

if LoadFreshData:
    pool=mp.Pool(nay)
    for nsc, sc in enumerate(Scenarios):
        for ncm, cm in enumerate(Models):
            start=time.time()
            print('Processing '+cm+', scenario=RCP'+sc+':')
            tasmin_full=load_full_timeseries("tasmin",cm,sc)
            tasmax_full=load_full_timeseries("tasmax",cm,sc)
            pr_full=load_full_timeseries("pr",cm,sc)
            #Implement mulitprocessor here.  First make partial function that adds fixed arguments in.
            #This is necessary because pool.map only takes one argument besides the actual function.
            #This argument holds the tuple of values that are fed to the function in a parallel manner.
            calc_climate_partial=partial(calc_climate,
                                 tasmin_full=tasmin_full,
                                 tasmax_full=tasmax_full,
                                 pr_full=pr_full)
            #Call the parallel job submitter.  This currently parallelizes over analysis years.  
            #So, the greater # of analysis years, the greater the potential for parallel gains, if
            #more CPUs added.
            out=np.asarray(pool.map(calc_climate_partial,ay))
            #'out' array arrives in form: analysis year, climate variable, window year, month.
            #Moving the analysis year index to end index, results in array shape where order of 
            #Climowin, months-in-year, and analysis year is same as Monthly* fields (though these
            #also contain cm, sc dimensions.  So, by marching thru climate variable dimension, 
            #Data from parallel processor can be merged into Monthly* arrays chuck-wise.
            out=np.moveaxis(out,0,-1)
            MonthlyTemp[:,ncm,nsc,:,:]=out[0,:,:,:]
            MonthlyPrecip[:,ncm,nsc,:,:]=out[1,:,:,:]
            MonthlyRainFrac[:,ncm,nsc,:,:]=out[2,:,:,:] #Currently in units of mm/day
            

            end=time.time()
            print("Processing time: "+str(end-start))

    #Clean up and convert some final details
    for m in range(0,12):
        MonthlyPrecip[:,:,:,m,:]=MonthlyPrecip[:,:,:,m,:]*DaysPerMonth[m]

    MonthlyRainFrac=np.clip(MonthlyRainFrac,0.,1.)
    MonthlyRainFrac=MonthlyRainFrac*100.
    print("Saving data...")
    np.savez("SavedAnalysisData.npz",MonthlyTemp=MonthlyTemp,
                                     MonthlyPrecip=MonthlyPrecip,
                                     MonthlyRainFrac=MonthlyRainFrac)
else:
    print("Reading data...")
    npzfile=np.load("SavedAnalysisData.npz")
    MonthlyTemp=npzfile['MonthlyTemp']
    MonthlyPrecip=npzfile['MonthlyPrecip']
    MonthlyRainFrac=npzfile['MonthlyRainFrac']

print("Generating output...")

start=time.time()
#Remove historical model-specific mean historical climate from each monthly projected climate
ds=(nMonthsinYear,nay)
##Temperature
dp50,dp55,dp67,dp75,dp85,FutProj50,FutProj55,FutProj67,FutProj75,FutProj85=calculate_projections(MonthlyTemp,ObsTemp,ds)
plot_seasonal_cycle(FutProj50,"Projected Mean",FutProj67,"Threshold Projection",ay,"Monthly temperature climatology (C)",FigPath,"Temp",-25.,20.)
generate_data_table(writer,FutProj67,"Temperature_Thresh_Proj",ay)

##Precipitation
dp50,dp55,dp67,dp75,dp85,FutProj50,FutProj55,FutProj67,FutProj75,FutProj85=calculate_projections(MonthlyPrecip,ObsPrecip,ds)
plot_seasonal_cycle(FutProj50,"Projected Mean",FutProj67,"Threshold Projection",ay,"Monthly precipitation climatology (mm)",FigPath,"Precip",0.,80.)
generate_data_table(writer,FutProj67,"Precipitation_Thresh_Proj",ay)

##Rain Fraction
dp50,dp55,dp67,dp75,dp85,FutProj50,FutProj55,FutProj67,FutProj75,FutProj85=calculate_projections(MonthlyRainFrac,ObsRainFrac,ds)
#Correct for fractional over/undershoots and fractional errors from skewed distribution
FutProj50=np.clip(FutProj50,0.,100.)
FutProj85=np.clip(FutProj85,0.,100.)

plot_seasonal_cycle(FutProj50,"Projected Mean",FutProj85,"Threshold Projection",ay,"Monthly rain fraction climatology (%)",FigPath,"RainFrac",0.,100.)
writer=generate_data_table(writer,FutProj85,"RainFrac_Thresh_Proj",ay)

end=time.time()
writer.save()

print("Processing time: "+str(end-start))


