import os
import numpy as np
import matplotlib.pyplot as plt
import calendar
from matplotlib.ticker import MaxNLocator
plt.switch_backend('agg')

for nyr in (20,30):
    iyr=26
    npzfile=np.load("SavedAnalysisData"+str(nyr)+"yr.npz")

    for Metric in ("precipitation","temperature","precipitation_as_rain"):
        if Metric=="precipitation":
            imon=6
            arr=npzfile['MonthlyPrecip'][:,:,:,imon,iyr]
        if Metric=="temperature":
            imon=6
            arr=npzfile['MonthlyTemp'][:,:,:,imon,iyr]
        if Metric=="precipitation_as_rain":
            imon=3
            arr=np.round(npzfile['MonthlyRainFrac'][:,:,:imon,iyr])
        MonthName=calendar.month_name[imon+1]
        #Calculate mean
        meanVal=np.mean(arr)
        #maxVal=np.amax(arr)
        maxVal=np.percentile(arr,99)
        pvalLevs=range(60,90)

        #Calculate varous P values
        pvals=np.percentile(arr,pvalLevs)
        npvals=len(pvals)
        
        #Populate a mean climatological window's worth of values
        SensArr=np.full((nyr),meanVal)
        threshMask=np.zeros((nyr+1,npvals))#+1, for 0 replacesments, all the way to all replacements
        threshBar=np.zeros((nyr+1))
        yrArr=np.zeros((nyr+1))
        for yr in range(nyr+1):
            yrArr[yr]=yr
            #For synthetic SensArr climatology, assess if thresholds passed
            for npv,p in enumerate(pvals):
                if SensArr.mean() > pvals[npv]:
                    threshBar[yr]=pvalLevs[npv]
                    threshMask[yr,npv]=1
            if yr < nyr:
                SensArr[yr]=maxVal

        FigPath="/home/garmeson_lanl/bucket_mount/sensitivity_tests/"

        img=plt.imshow(threshMask,origin='lower')
        ax=plt.gca()
        ax.set_aspect('auto')
        img.set_cmap("bwr")
        plt.xticks(range(0,npvals,5),pvalLevs[0:-1:5])
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        plt.grid()
        plt.text(0.05,0.92,'Exceeded',color='red',transform=ax.transAxes,backgroundcolor='white')
        plt.text(0.78,0.05,'Not exceeded',color='blue',transform=ax.transAxes,backgroundcolor='white')
        plt.xlabel("Climate event threshold percentile (P values)")
        plt.ylabel("# of extra P99 (1-in-100) events\nin window")
        plt.title(MonthName+" monthly "+Metric+"\n"
                   +str(nyr)+" year window ending in 2040")
        plt.savefig(os.path.join(FigPath,"sensitivity"+Metric+str(nyr)+".png"),dpi=300)
        plt.close("all")
