#!/bin/bash

f='/home/garmeson_lanl/bucket_mount/figs_PA3.0'

for v in Temp Precip RainFrac; do
    p="$f"/"$v"
    files=`ls "$p"2025.png "$p"2030.png "$p"2035.png "$p"2040.png "$p"2045.png "$p"2050.png`
    montage -mode concatenate -tile 2x3 $files miff:- | convert miff:- -resize 2048x2048 $f/Montage$v.png
done
#montage $files $FigDir/TempMontage.png
